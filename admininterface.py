# [ ] tratar se a entrada do id de cargo ou partido não for uma numero
# [ ] quando for cadastrado um partido ou cargo, informar o id

## admin.py
import pickle
from candidato import *
from partido import *
from eleitor import *
from cargo import *
from myscript import *

## Interface para configuração da eleição.
## Permite o cadastro de cargos, partidos, candidatos
## e eleitores.

## As listas de cargos, partidos, candidatos e eleitores devem conter
## objetos das respectivas classes.

## Na lista de candidatos, o primeiro objeto armazena os votos nulos, e o segundo
## os votos em branco.

## Nas linhas marcadas com ### está faltando alguma coisa. Preencha essas lacunas.

def main():
    msg = """
+----------------------------------------------------------------+
+---------- Interface para configuração da eleição --------------+
|    Digite um dos comandos abaixo:                              |
|    * cadastrarcandidato                                        |
|    * cadastrarcargo                                            |
|    * cadastrarpartido                                          |
|    * cadastrareleitor                                          |
|    * boletimdeurna                                             |
|    * sair                                                      |
+----------------------------------------------------------------+
"""
    print(msg)
    while True:
        comando = input(">>> Instruções: ")
        if comando == "cadastrarcandidato":
            ## inserir um novo objeto Candidato na lista de candidatos.
            ###
            try:
                arquivo = open(caminho_listacand,"br")
                listacandidatos = pickle.load(arquivo)
                arquivo.close()
            except:
                listacandidatos = [Candidato('Nulo',0,0,0),Candidato('Branco','',0,0)]
            ###
            print("Entre com as informações do novo candidato... ")
            nome    = input("Nome: ")
            numero  = myinput("Numero: ", toInt=True)
            cargo   = myinput("Id do cargo: ", toInt=True)
            partido = myinput("Id do partido: ", toInt=True)

            novocandidato = Candidato(nome,str(numero),int(cargo),int(partido))
            listacandidatos.append(novocandidato)
            ###
            arquivo = open(caminho_listacand,"bw")
            pickle.dump(listacandidatos,arquivo)
            arquivo.close()
            ###
            
        elif comando == "cadastrarpartido":
            ## inserir um novo objeto Partido na lista de partidos.
            ###
            try:
                arquivo = open(caminho_listapart,"br")
                listapartidos = pickle.load(arquivo)
                arquivo.close()
            except:
                listapartidos = []
            ###
            print("Entre com as informações do novo partido: ")
            nome = input("Nome: ")
            sigla = input("Sigla: ")
            novopartido = Partido(nome,sigla)
            listapartidos.append(novopartido)
            ###
            arquivo = open(caminho_listapart, "bw")
            pickle.dump(listapartidos, arquivo)
            arquivo.close()
            ###

        elif comando == "cadastrareleitor":
            ## inserir um novo objeto Eleitor na lista de eleitores.
            try:
                arquivo = open(caminho_listaelei,"br")
                listaeleitores = pickle.load(arquivo)
                arquivo.close()
            except:
                listaeleitores = []
            ###
            print("Entre com as informações do novo eleitor: ")
            nome = input("Nome: ")
            registro = input("Registro: ")
            novoeleitor = Eleitor(nome,registro)
            listaeleitores.append(novoeleitor)
            ###
            arquivo = open(caminho_listaelei,"bw")
            pickle.dump(listaeleitores,arquivo)
            arquivo.close()
            
        elif comando == "cadastrarcargo":
            ## inserir um novo objeto Cargo na lista de cargos.
            try:
                arquivo = open(caminho_listacarg,"br")
                listacargos = pickle.load(arquivo)
                arquivo.close()
            except:
                listacargos = []
            ###
            print("Entre com as informações do novo cargo: ")
            nome = input("Nome: ")
            novocargo = Cargo(nome)
            listacargos.append(novocargo)
            ###
            arquivo = open(caminho_listacarg,"bw")
            pickle.dump(listacargos,arquivo)
            arquivo.close()
            
        elif comando == "sair":
            break
        
        elif comando == "boletimdeurna":
            ## exibir o boletim de urna, com a contagem dos votos.
            arquivo = open(caminho_listacand,"br")
            listacandidatos = pickle.load(arquivo)
            arquivo.close()
            arquivo = open(caminho_listaelei,"br")
            listaeleitores = pickle.load(arquivo)
            arquivo.close()
            contador = listacandidatos[1].contarVotos() + listacandidatos[0].contarVotos()
            string = "********* Boletim de urna *********\n\n" + "Votos em branco      : " +\
                     str(listacandidatos[1].contarVotos()) + "\n" +\
                     "Votos nulos          : " + str(listacandidatos[0].contarVotos()) + "\n"
            for candidato in listacandidatos[2:]:
                string = string + candidato.resultado() + "\n"
                contador = contador + candidato.contarVotos()
            string = string + "\n" +\
                     "Eleitores cadastrados: " + str(len(listaeleitores)) + "\n" +\
                     "Total de votos       : " + str(contador) + "\n" +\
                     "Abstenções           : " + str(len(listaeleitores) - contador) + "\n\n***********************************"
            print(string)
        else:
            print("Comando inválido.")

if __name__ == "__main__":
    main()
