## candidato.py
import pickle
from partido import *
from cargo import *

## Nomes dos arquivos que armazenam as listas de candidatos,
## cargos, partidos e eleitores.

## lista de candidatos
caminho_listacand = "listacandidatos"

## lista de cargos
caminho_listacarg = "listacargos"

## lista de partidos
caminho_listapart = "listapartidos"

## lista de eleitores
caminho_listaelei = "listaeleitores"

class Candidato:
    def __init__(self,nome,numero,idcargo,idpartido):
        ## idcargo representa o índice, na lista de cargos, do cargo
        ## ao qual o candidato está concorrendo.
        ## idpartido representa o índice, na lista de partidos, do partido
        ## do candidato.
        self.__nome = str(nome)
        self.__numero = str(numero)
        self.__idCargo = idcargo
        self.__idPartido = idpartido
        self.__votos = 0

    def __repr__(self):
        string = self.__nome + " - " + self.__numero
        return string

    def computarvoto(self):
        self.__votos = self.__votos + 1

    def info(self):
        #TRATA ABERTURA DE ARQUIVO
        try:
            arquivo1 = open(caminho_listapart,"br")
            listapartidos = pickle.load(arquivo1)
            arquivo1.close()
        except:
            listapartidos = [Partido('Sem partido','')]
        
        try:
            arquivo2 = open(caminho_listacarg,"br")
            listacargos = pickle.load(arquivo2)
            arquivo2.close()
        except:
            listacargos = [Cargo('Sem cargo')]
        

        # TRATA SE O idPardito OU O idCargo ESTÃO DENTRO DE LIMITE
        if self.__idPartido < 0 or self.__idPartido >= len(listapartidos):
            self.__idPartido = 0
        if self.__idCargo < 0 or self.__idCargo >= len(listacargos):
            self.__idCargo = 0

        string = "Nome: " + self.__nome + "\n" +\
                 "Número: " + self.__numero + "\n" +\
                 listapartidos[self.__idPartido].info() + "\n" +\
                 listacargos[self.__idCargo].info()
        return string

    def contarVotos(self):
        return self.__votos

    def resultado(self):
        string = self.__nome
        for i in range(21-len(self.__nome)):
            string = string + " "
        string = string + ": " + str(self.__votos)
        return string
