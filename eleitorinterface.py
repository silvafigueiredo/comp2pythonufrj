## ESSA É UMA LISTA DE COISAS A SEREM FEITAS
## [x] 1-Quando abrir lista de cadidatos e o arquivo não existir, criar lista com o cardidato 'Nulo' e 'Branco'
## [ ] 2-Os indices zero para cargo e partido do candidato vão indicar 'Sem cargo' e 'Sem partido' respectivamente
## [X] 3-Se lista de eleitores estiver vazia ou arquivo inexistente, o eleitor 'Anônimo será criado' com o registro 20.000
## [ ] 4-Se uma consulta aos dados do candidato for feita e os indices de cargo e partido não existirem será atribuido
##       'Sem cargo' e 'Sem partido' respectivamente

## eleitorInterface.py

import pickle
import sys              #módulo utilizado para exibir detalhes de erros
from candidato import *
from partido import *
from eleitor import *
from cargo import *

## Interface para o eleitor. Simula uma 'urna eletrônica', onde o eleitor se identifica
## e registra o seu voto.

def main():
    #UMA PEQUENA MARQUINA DE ESTADOS
    
    #ABRE ARQUIVOS COM LISTA DE CADIDADOS E ELEITORES
    try:
        arquivo1 = open(caminho_listacand,"br")
        listacandidatos = pickle.load(arquivo1)
        arquivo1.close()
    except:
        arquivo = open(caminho_listacand,"bw")        
        arquivo.close()
        listacandidatos = [Candidato('Nulo',0,0,0),Candidato('Branco','',0,0)]
        

    try:
        arquivo2 = open(caminho_listaelei,"br")
        listaeleitores = pickle.load(arquivo2)
        arquivo2.close()
    except:
        arquivo = open(caminho_listaelei,"bw")
        arquivo.close()
        listaeleitores = [Eleitor('Anônimo',20000)]
    
    #MENSAGEM DE CABEÇALHO
    msg = """
*************************************************************
***** Simulador Rudimentar de Votação Eletrônica (SRVE) *****

           NÃO UTILIZE EM UMA ELEIÇÃO REAL!!!
*************************************************************
"""
    print(msg)

    #PEDE A IDENTIFICAÇÃO DO ELEITOR TODO:O QUE É FEITO COM ESSA IDENTIFICAÇÃO?
    print(">>> Identificação do eleitor...\n")
    eleitor_registrado = False
    while True:
        registro = input(">>> Registro do eleitor: ")
        #VERIFICA SE O ELEITOR ESTA CADASTRADO
        for eleitor in listaeleitores:
            if eleitor._Eleitor__registro == registro:
                eleitor_registrado = True
                break
        if eleitor_registrado == False:
            print("Registro não encontrado, tente de novo")
        else:
            break;
    
    #ESCOLHER E CONFIRMAR CANDIDATO
    print(listacandidatos)
    confirma = 'n'
    while confirma == 'n':
        escolhadoeleitor = input(">>> Digite o número do seu candidato: ")
        for candidato in listacandidatos:
            if candidato._Candidato__numero == escolhadoeleitor:
                print(candidato.info())
                confirma = input(">>> Confirmar voto? (s/n)")
                if confirma == 's':
                    candidato.computarvoto()
                break
    
    #INFORMA O ELEITOR QUE O VOTO FOI CONCLUÍDO
    print('OBRIGADO POR VOTAR!')

    #SALVA MODIFICAÇÕES NAS LISTAS DE CADIDADOS/ELEITORES
    arquivo1 = open(caminho_listacand,"bw")
    pickle.dump(listacandidatos,arquivo1)
    arquivo1.close()
    arquivo2 = open(caminho_listaelei,"bw")
    pickle.dump(listaeleitores,arquivo2)
    arquivo2.close()

if __name__ == "__main__":
    main()
